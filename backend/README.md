# zigong

## Build Setup

```bash
# install dependencies
$ npm install

# install imagemagick and ghostscript
$ sudo apt-get install imagemagick
$ sudo apt-get install ghostscript

# serve with hot reload at localhost:3000
$ npm run dev

# production
$ pm2 start

# pdf-thumbnail - how to fix bug on linux
https://github.com/nicofuccella/pdf-thumbnail/issues/4

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
```
