"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "Files",
      [
        {
          thumbnail: "G1ASjM2JelgQhfROKB6yJTkNj.webp",
          name: "zigong-texas-1.webp",
          description: null,
          file: "seya9EDRNFuyrMfid1TQ7ZsuS.webp",
          mimetype: "png",
        },
        {
          thumbnail: "4tI5bBvirPi15hcRwKpoJfKF2.webp",
          name: "SLIDE5.jpg",
          description: null,
          file: "iZm3ChAxVq4dEH9HxEsOBYpHo.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "M6DxaUDNZiAvcvepMIC6iSiw5.webp",
          name: "Slider-wmo.jpg",
          description: null,
          file: "spDudw8tKWKzapw9XUPPbEJaI.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "zzXU0U8vaFNaWi2LALV0oGbHX.webp",
          name: "SLIDE1.jpg",
          description: null,
          file: "bj1o1314i7LPDJInsXxpfQ6Tj.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "DS2BbhpV7CJrqeNz1DK25lQPq.webp",
          name: "SLIDE2.jpg",
          description: null,
          file: "6u60MeTjB8Mf1XPGVahQrkEoS.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "5MIlZxVytmU89SG2gc2Kc0vEf.webp",
          name: "SLIDE3.jpg",
          description: null,
          file: "e1vMFwm4F2u9LZrN64QqdOJe8.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "p3BjYuCXHSay6K41DK56GXzdl.webp",
          name: "powders.png",
          description: null,
          file: "HXAsh1iF2NIqEEgnbzblAwvT5.webp",
          mimetype: "png",
        },
        {
          thumbnail: "pgR6FhqvrpDUMUoZoPj0d1GUQ.webp",
          name: "products-powders.jpg",
          file: "TGIIzRrwNJsrHFGfSI6fb0Tey.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "GbHZUYNiT9deSyb1Z1AVdtHZl.webp",
          name: "2-cemented-carbides-home-page.png",
          file: "TmOenRlsG3rXYocFYJCc8fBUL.webp",
          mimetype: "png",
        },
        {
          thumbnail: "YM1uNmd1HMMXWdGnQiWiwKngG.webp",
          name: "products-cemented-carbides.jpg",
          file: "LKFxZ8z2KdWtFFNcRELzsBi3g.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "JuVKAczMrfsfM771kiRcaJVHj.webp",
          name: "hard-facing-powder-a.png",
          file: "IMwyLXmvL0e3k6b7VAgyeQ4RO.webp",
          mimetype: "png",
        },
        {
          thumbnail: "X8QhvP2QzYhMkQ3GDC394ZPIL.webp",
          name: "products-hardfacing-material.jpg",
          file: "kDOIFsaI6uXOl16OyKZVy2AaI.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "Cn6gNG9yIVWl6neyvOWl2SDyg.webp",
          name: "products-wmo-products.jpg",
          file: "3FcxqzrLWqBpNpxFj56Iysch8.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "ia4FfBxFGIR1YpFKtx0dmwKWZ.webp",
          name: "w-mo-products-homepage.jpg",
          file: "bRsXuOufbsMWkD5YZkqWZ7mhU.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "uSsfVh7WvxwyVsoDwB4qxEWbd.webp",
          name: "equipment-a.png",
          file: "v5srMXejDFsngtGbLtsPKWKe2.webp",
          mimetype: "png",
        },
        {
          thumbnail: "cTVoneXMLeSNuHIX2sOl6TJ79.webp",
          name: "products-equipment.jpg",
          file: "EwKEjNNUSSAJBG97vtFWgzdZA.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "zKnzNk4NdVPXZsPlRMLyciell.webp",
          name: "pobrane.jpeg",
          file: "muXYiOSIFbnGOplvHROGE0WVF.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "QXNQ8G6SIONhzO6C9sQdLEPZY.webp",
          name: "pobrane (1).jpeg",
          file: "nkmHixBTYe4WYWpZZwXz2lHZr.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "2VQFv6crxewmNG6QeahjpiiOK.webp",
          name: "pobrane (2).jpeg",
          file: "vBWYwZFmVkF8yuZJFYN5IYGkW.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "nxuFMFi6BhGHGrQnkfX4QbO1K.webp",
          name: "pobrane (3).jpeg",
          file: "DsZJSqLPOBD6IpnvJQQNT9oPY.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "0eD2nz5MGIOwCIJTs8iBbZnNZ.webp",
          name: "pobrane (1).jpeg",
          file: "MaC2A5L4WSK1vkxXHfXyyTOvg.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "iCYM2zE9Gf2wYANU0QBRZwvIF.webp",
          name: "pobrane (2).jpeg",
          file: "0NeOUqAQtHXaIh19hTF15myzO.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "sNXEkQrEZcWhowYhJ3wD3Q0sj.webp",
          name: "pobrane (3).jpeg",
          file: "Oi27eIf51BD1yJerU6XV9pF8d.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "FGgbwsGiTqIF5G5iUrXmcoRN2.webp",
          name: "pobrane (4).jpeg",
          file: "wm282jpArZmFBR1rOay701gXj.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "hs4ytt8R4yZWhwenA8fEw3rBS.webp",
          name: "pobrane (5).jpeg",
          file: "9USPWJ0fLoTVXjylQUN8ec8Hh.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "i2Udl04q0Gxi9rT6xVyahGZhV.webp",
          name: "pobrane (6).jpeg",
          file: "SM7MiX1jvqdClyll5HWZ9Eo3k.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "eWcvUuZ7Ck5nYcTwTjo54IoNt.webp",
          name: "pobrane (7).jpeg",
          file: "7kuZrYN2ZJa3zv8ArnebP0YjH.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "IpLQaObCrXBZJ34vrqN9aZfbW.webp",
          name: "pobrane (8).jpeg",
          file: "rCrTtH00o7jX9z33ZJknJRcfD.webp",
          mimetype: "jpeg",
        },
        {
          thumbnail: "EYWpzlJo6tK6cN3RcakpWk0br.webp",
          name: "pobrane.jpeg",
          file: "uXAHiYZlEnjOwiNUC5FE4wBYX.jpeg",
          mimetype: "jpeg",
        },
        {
          thumbnail: "hnJbTg33RBpgMhtQ5qd4YpiYl.webp",
          name: "CNAS-1-1.pdf",
          description: "CNAS",
          file: "JGzM98fs0qrxXItGTcpnLFpng.pdf",
          mimetype: "pdf",
        },
        {
          thumbnail: "gZruu7TmZgpBEXfX5eHiW7aJj.webp",
          name: "ISO-9001-1.pdf",
          description: "ISO-9001",
          file: "M3bSlHoZ1fqOzpppIiVrv7fro.pdf",
          mimetype: "pdf",
        },
        {
          thumbnail: "Y1ehJrAY4BWnwFKogsOh9l2tF.webp",
          name: "ISO-14001-2.pdf",
          description: "ISO-14001",
          file: "tLRAJWlqmzOXYsGi6wETDZF2Q.pdf",
          mimetype: "pdf",
        },
        {
          thumbnail: "gIdWVSS72WuU2qyHlZfN04pda.webp",
          name: "ISO-45001-1.pdf",
          description: "ISO-45001",
          file: "5I3mLvVmTDx5MO52vQ1F9ptzi.pdf",
          mimetype: "pdf",
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {},
};
