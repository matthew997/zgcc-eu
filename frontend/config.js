module.exports = {
    apiBaseUrl: process.env.apiBaseUrl,
    mediaBaseUrl: process.env.mediaBaseUrl,
    appUrl: process.env.appUrl
};
